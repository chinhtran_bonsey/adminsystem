import express from 'express'; 
var loginGet = express.Router();
loginGet.get('/login', function(req, res) {  
  res.render('logintpl', {
    //layout:'login.hbs',
    title: `AdminSystem | Log in`,
    logo: '<b>Admin</b>System',
    message:'Sign in to start your session',
    rememberAcc: `Remember Me`,
    signIn:`Sign In`,
    forgotPass: `I forgot my password`,
    regNewMem:`Register a new membership`,
    csrfToken : req.csrfToken(),
    email:'Email',
    password:'Password'
  });
}); 
module.exports = loginGet;
