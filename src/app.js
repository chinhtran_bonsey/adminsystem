import express from 'express';
import gulp from 'gulp';
import session from 'express-session';
import path from 'path';
import exphbs from 'express-handlebars'; 
import cookieParser from 'cookie-parser';
import csrf from 'csurf';
import bodyParser from 'body-parser';
import url from 'url'; 
var csrfConfig = require('./config/csrf-exclude'); 
var app = express(); 

var viewsPath = path.join(__dirname,'views');
app.set('views', viewsPath);
app.engine('.hbs', exphbs({
    defaultLayout: 'main',
    extname:'hbs', 
    layoutsDir: viewsPath + '/layouts'
    /*layoutsDir: path.join(__dirname, 'views' , 'layouts'),
    partialsDir:path.join(__dirname, 'views ' , 'partials')*/
}));
app.set('view engine', 'hbs'); 
app.set('port', process.env.PORT || 3000);
var options = { dotfiles: 'ignore', etag: false,
    extensions: ['htm', 'html'],
    index: false
}; 

app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(session({secret: 'dasd$#$d'}));
//global.csrfProtection = csrf({ cookie: true });   
global.parseForm = bodyParser.urlencoded({ extended: false }); 
app.use(express.static(path.join(__dirname, 'public') , options)); 
//app.use(express.static(path.join(__dirname, 'public') , options),csrfProtection,parseForm); 
app.use(function (req, res, next) {    
     let path = url.parse(req.url).pathname.replace("/",""); 
     if(csrfConfig.hasOwnProperty(path)) {        
        global.csrfProtection = csrf({ cookie: true });            
        app.use(csrfProtection,parseForm);   
     }else{
        global.csrfProtection = csrf({ cookie: true });            
        app.use(csrfProtection,parseForm);  
        console.log("chinh1");  
     }
     
     next();
}); 
global.csrfProtection = csrf({ cookie: true });            
app.use(csrfProtection,parseForm);  
 
app.use('/', require('./controllers/post/login')); //login post 
app.use('/', require('./controllers/get/main')); //mainpage
app.use('/', require('./controllers/get/login')); //login page     
 


app.listen(app.get('port'),  function () {    
    console.log('Server started on http://localhost:' +
    app.get('port') + '; press Ctrl-C to terminate.' );     
});



 